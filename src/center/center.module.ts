/**
 * Created by liufeng on 2018/1/18.
 */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { CenterComponent } from './center.component';


@NgModule({
  declarations: [
    CenterComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [CenterComponent]
})
export class CenterModule { }
