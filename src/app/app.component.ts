import { Component, ComponentRef, ViewChild, ComponentFactoryResolver } from '@angular/core';
import { DlHostDirective } from '../center/dl-host.directive';
import { CenterComponent } from '../center/center.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  component: ComponentRef<CenterComponent>;
  @ViewChild(DlHostDirective) dlHost: DlHostDirective;
  constructor(private componentFactoryResolver: ComponentFactoryResolver) { }

  createCenter() {
    this.component = this.dlHost.viewContainerRef.createComponent(
      this.componentFactoryResolver.resolveComponentFactory(CenterComponent)
    );
  }
  centerDispose() {
      this.component.destroy();
  }
  ngOnInit() {
    console.log("-----------------ngOnInit-------------------");
    var viewer = new Cesium.Viewer('cesiumContainer');
  }

}
