import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {HeaderComponent} from '../header/header.component';
import {CenterComponent} from '../center/center.component';
import {HeroComponent} from '../hero/hero.component';
import { Injector } from '@angular/core';

import { DlHostDirective } from '../center/dl-host.directive';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CenterComponent,
    DlHostDirective,
    HeroComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  entryComponents: [CenterComponent],
  bootstrap: [AppComponent, HeaderComponent, HeroComponent]
})
export class AppModule  {
  run() {
    const injector2 = Injector.resolveAndCreate([Car,
      Engine, Doors, Body]);
    const car = injector.get(Car);
    car.run();
  }
}

